<?php

/* Una web española de servicios tiene un pequeño problema, quiere que indiquemos en su página de inicio el día de
 * la semana, tarea sencilla de no ser por que el servidor nos proporciona el día en inglés mediante la función date().
 * 
 * 
 * Ayuda del ejercicio:
1. La función date() con el parametro 'D' nos devuelve las tres primeras letras del día de la semana en inglés: Mon, Tue, Wed, Thu, Fri, Sat, Sun.
2. Con este pequeño código $dia_ingles = date('D'); tendremos el día en la variable $dia_ingles.
3. Mostrar el día en español por pantalla con el mensaje: "El día de la semana es: XXXX".
 * 
 * 
 * 
*/

$dia_ingles = date('D');

if ($dia_ingles == "Mon") {
    echo "Hoy es dia: Lunes";
} else if ($dia_ingles == "Tue") {
    echo "Hoy es dia: Martes";
} else if ($dia_ingles == "Wed") {
    echo "Hoy es dia: Miercoles";
} else if ($dia_ingles == "Thu") {
    echo "Hoy es dia: Jueves";
} else if ($dia_ingles == "Fri") {
    echo "Hoy es dia: Viernes";
} else if ($dia_ingles == "Sat") {
    echo "Hoy es dia: Sabado";
} else {
    echo "Hoy es Domingo";
}
